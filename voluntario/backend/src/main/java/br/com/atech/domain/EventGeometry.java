package br.com.atech.domain;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

@Data
public class EventGeometry {

	private LocalDate date;
	private String type;
//	private Object coordinates;
	private List<Object> coordinates;
}
