package br.com.atech.domain;

import lombok.Data;

@Data
public class EventCategory {

	private Integer id;
	private String title;
	
}
