package br.com.atech.domain;

import lombok.Data;

@Data
public class Pista {

	
	private String id;
	private GeometryPista geometry;
	private PistaProperties properties;
}
