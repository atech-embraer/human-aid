package br.com.atech.domain.infraestructure;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.atech.domain.CollectionPista;
import br.com.atech.domain.Pista;
import br.com.atech.domain.repository.PistaRepository;

@Repository
public class PistaRepositoryImpl implements PistaRepository {

	@Override
	public List<Pista> listarPistas() {
		  	URI uri = null;
	        try {
	            uri = new URI("https://geoservicos.ibge.gov.br/geoserver/CCAR/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=CCAR:BCIM_Pista_Ponto_Pouso_P&outputFormat=JSON");
	            RestTemplate rest =new RestTemplate();
	            CollectionPista body = rest.getForEntity(uri, CollectionPista.class).getBody();
	            return body.getFeatures();
	            
	        } catch (final Exception e) {
	        	e.printStackTrace();
	        }
		
	        
		return new ArrayList<>();
	}

	
	
}
