package br.com.atech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.atech.domain.Pista;
import br.com.atech.domain.repository.PistaRepository;

@RestController
@RequestMapping("/hackaton/pistas")
public class PistaController {

	
	@Autowired
	PistaRepository repository;
	
	@GetMapping
	public List<Pista> listar(){
		return repository.listarPistas();
	}
	
	
}
