package br.com.atech.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PointInfo {
	
	private String name;
	private double latitude;
	private double longitude;

}
