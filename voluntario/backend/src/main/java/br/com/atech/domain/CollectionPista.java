package br.com.atech.domain;

import java.util.List;

import lombok.Data;

@Data
public class CollectionPista {

    String type;
    private Integer totalFeatures;
    private List<Pista> features;

	
}
