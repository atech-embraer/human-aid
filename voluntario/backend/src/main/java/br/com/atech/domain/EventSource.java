package br.com.atech.domain;

import lombok.Data;

@Data
public class EventSource {
	
	private String id;
	private String url;

}
