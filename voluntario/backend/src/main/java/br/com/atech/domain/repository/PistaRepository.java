package br.com.atech.domain.repository;

import java.util.List;

import br.com.atech.domain.Pista;

public interface PistaRepository {
	
	List<Pista> listarPistas();

}
