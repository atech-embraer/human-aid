package br.com.atech.domain;

import java.util.List;

import lombok.Data;

@Data
public class GeometryPista {

	private List<Double> coordinates;
	
}
