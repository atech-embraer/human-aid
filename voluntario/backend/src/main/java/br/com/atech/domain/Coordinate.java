package br.com.atech.domain;

import lombok.Data;

@Data
public class Coordinate {

	private double coordinate;
	
}
