package br.com.atech.domain.infraestructure;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.atech.domain.Alert;
import br.com.atech.domain.repository.PointInfoRepository;

@Repository
public class PointInfoRepositoryImpl implements PointInfoRepository {

	@Override
	public List<Alert> listPoints() {
		
		List<Alert> points = new ArrayList<>();
		
			points.add(new Alert("HIGH_RISK",-3.385109d,36.686851d));
			points.add(new Alert("HIGH_RISK",-3.382067d,36.688542d));
			points.add(new Alert("HIGH_RISK",-3.380905d,36.688349d));
			
		return points;
	}
	
	@Override
	public List<Alert> listSecurePoints() {
		
		List<Alert> points = new ArrayList<>();
		
			points.add(new Alert("SECURE",-3.386570, 36.701739));
//			points.add(new Alert("HIGH_RISK",-3.382067d,36.688542d));
//			points.add(new Alert("HIGH_RISK",-3.380905d,36.688349d));
			
		return points;
	}

}
