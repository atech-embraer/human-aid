package br.com.atech.domain.repository;

import java.util.List;

import br.com.atech.domain.Alert;

public interface PointInfoRepository {

	public List<Alert> listPoints();

	public List<Alert> listSecurePoints();
	
}
