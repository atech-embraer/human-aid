package br.com.atech.domain;

import java.util.List;

import lombok.Data;

@Data
public class Event {

	private String id;
	private String title;
	private String decription;
	private String link;
	private List<EventCategory> categories;
	private List<EventSource> sources;
	private List<EventGeometry> geometries;
}
