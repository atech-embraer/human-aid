package br.com.atech.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Alert {

	private String type;
	private double lat;
	private double lng;
}
