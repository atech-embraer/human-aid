package br.com.atech.domain;

import lombok.Data;

@Data
public class PistaProperties {

	
	private Integer largura;
	private Double extensao;
	private String operacional;
	private String situacaofisica;
	private String tipopista;
	private String usopista;
	private String revestimento;
	private String nomeabrev;

}
