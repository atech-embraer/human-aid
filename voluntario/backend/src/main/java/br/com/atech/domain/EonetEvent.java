package br.com.atech.domain;

import java.util.List;

import lombok.Data;

@Data
public class EonetEvent {

	private String title;
	private String description;
	private String link;
	private List<Event> events;
}
