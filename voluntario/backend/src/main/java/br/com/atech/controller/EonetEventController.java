package br.com.atech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.atech.domain.EonetEvent;
import br.com.atech.service.EventService;

@RestController
@RequestMapping("/hackaton/events")
public class EonetEventController {

	@Autowired
	EventService eventService;
	
	
	@GetMapping
	public EonetEvent list() {
		return eventService.getEvents();
	}
	
}
