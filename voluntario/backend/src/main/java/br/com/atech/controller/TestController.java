package br.com.atech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.atech.domain.Alert;
import br.com.atech.domain.repository.PointInfoRepository;

@RestController
@RequestMapping("/hackaton")
public class TestController {

	
	
	@Autowired
	PointInfoRepository repository;
	
	@GetMapping("/points")
	public List<Alert> listar(){
		return repository.listPoints();
	}
	
	@GetMapping("/secure-points")
	public List<Alert> securePoints(){
		return repository.listSecurePoints();
	}
	
	
}
