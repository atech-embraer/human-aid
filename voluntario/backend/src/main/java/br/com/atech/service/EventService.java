package br.com.atech.service;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.atech.domain.EonetEvent;

@Service
public class EventService {

    private final Logger logger = LoggerFactory.getLogger(EventService.class);

	public EonetEvent getEvents() {
		
	  	URI uri = null;
        try {
            uri = new URI("https://eonet.sci.gsfc.nasa.gov/api/v2.1/events");
            RestTemplate rest =new RestTemplate();
            EonetEvent body = rest.getForEntity(uri, EonetEvent.class).getBody();
            return body;
            
        } catch (final Exception e) {
        	logger.error("Erro ao recueprar EONET Event", e);
        }
        
        return new EonetEvent();
		
	}
	
}
