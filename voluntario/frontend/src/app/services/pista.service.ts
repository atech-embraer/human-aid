import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PistaService {

  constructor(private http: HttpClient) { }


  listAllPistas(): Promise<Array<any>> {
    return this.http.get<Array<any>>(
      `${environment.BASE_URL}/hackaton/pistas`
    ).toPromise().then( resp => {
      console.log(resp);
      return resp;
    } );
  }
}
