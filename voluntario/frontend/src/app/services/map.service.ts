import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LatLong } from '../model/lat-long';
import { environment } from 'src/environments/environment';
import { Polygon } from '../model/area';
import { Alert } from '../model/alert';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }


  getPointsMonitored() {
    return this.http.get<Array<Alert>>(
      `${environment.BASE_URL}/hackaton/points`
    ).toPromise().then( resp => {

      return resp;
    } );
  }

  getSecurePointsMonitored() {
    return this.http.get<Array<Alert>>(
      `${environment.BASE_URL}/hackaton/secure-points`
    ).toPromise().then( resp => {

      return resp;
    } );
  }

  getPolygin() {
    return this.http.get<any>(
      `${environment.BASE_URL}/vermelho`
    ).toPromise().then( resp => {
      console.log(resp);
      return resp;
    } );
  }


  getPolygons() {
    return this.http.get<Array<Polygon>>(
      `${environment.BASE_URL}/polygons`
    ).toPromise().then( resp => {
      console.log(resp);
      return resp;
    } );
  }

}
