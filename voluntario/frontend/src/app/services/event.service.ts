import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { EonetEvent } from '../model/eonet-event';
import { LatLong } from '../model/lat-long';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }


  getLocalMonitored() {
    return this.http.get<LatLong>(
      `${environment.BASE_URL}/hackaton/events`
    ).toPromise().then( resp => {
      console.log(resp);
      return resp;
    } );
  }


}
