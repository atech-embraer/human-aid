import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PointInfo } from '../point-info';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PointInfoService {

  constructor( private http: HttpClient) {}


  listAllPoints(): Promise<Array<PointInfo>> {
    return this.http.get<Array<PointInfo>>(
      `${environment.BASE_URL}/hackaton/points`
    ).toPromise().then( resp => {
      console.log(resp);
      return resp;
    } );
  }
}
