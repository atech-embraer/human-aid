import { Component, OnInit } from '@angular/core';
import { PointInfo } from './point-info';
import { PointInfoService } from './services/point-info.service';
import { PistaService } from './services/pista.service';
import { EventService } from './services/event.service';
import { Event } from './model/event';
import { Geometry } from './model/fakeevent';
import { EventGeometry } from './model/event-geometry';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit {
  events: Array<Event>;
  eventsGeometry: Array<Geometry>;

  title = 'hackaton';

  constructor(private eventService: EventService,
    private pistaService: PistaService) {
    // this.loadEvents();


   }

  loadEvents() {
    // console.log("aaa");
    //   this.eventService.listAllEvents().then( resp => {
    //     // console.log('EONET event', resp);
    //     this.events = resp.events;
    //     this.eventsGeometry = new Array<Geometry>();
    //     this.events.forEach( event => {

    //       console.log(event.id);

    //       if( event.geometries[0].type !== 'Polygon'){

    //         const x = event.geometries[0].coordinates[0];
    //         const y = event.geometries[0].coordinates[1];
    //         this.eventsGeometry.push(new Geometry( x, y));
    //       }else{

    //       }



    //     });



    //   });
   }

   getIconUrl(event: Event): String{

    //Wildfires
    if( event.categories[0].id === 8) {
      return '/assets/fire-alt-solid.svg';
    }

    //Snow
    if( event.categories[0].id === 17) {
      return '/assets/snowflake-solid.svg';
    }

    //Severe Storms
    if( event.categories[0].id === 10) {
      return '/assets/poo-storm-solid.svg';
    }

    //volcones
    if( event.categories[0].id === 12) {
      return '/assets/burn-solid.svg';
    }

    //volcones
    if( event.categories[0].id === 15) {
      return '/assets/icicles-solid.svg';
    }


    return '/assets/cloud-showers-heavy-solid.svg';


   }



  ngOnInit() {
  }

}
