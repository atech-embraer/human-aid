export class EventGeometry {
  date: Date;
  type: String;
  coordinates: Array<Number>;
}
