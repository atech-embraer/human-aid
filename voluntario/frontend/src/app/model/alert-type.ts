export enum AlertType {

  HIGH_RISK, MODERATE_RISK, LOW_RISK, SAFE
}
