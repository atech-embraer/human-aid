import { Event } from './event';

export class EonetEvent{

  title: String;
  description: String;
  link: String;
  events: Array<Event>;

}
