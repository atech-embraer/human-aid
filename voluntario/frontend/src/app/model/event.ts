import { EventCategory } from "./event-category";
import { EventGeometry } from "./event-geometry";

export class Event {

  id: String;
  title: String;
  description: String;
  link: String;
  categories: Array<EventCategory>;
  sources: Array<EventSource>;
  geometries: Array<EventGeometry>;

}
