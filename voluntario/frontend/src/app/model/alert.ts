import { AlertType } from './alert-type';

export class Alert {

  lat: Number;
  lng: Number;
  type: AlertType;

}
