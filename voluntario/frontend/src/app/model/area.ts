import { LatLong } from './lat-long';
import { AlertType } from './alert-type';

export class Polygon {

  paths: Array<LatLong> = new Array<LatLong>();
  alertType: AlertType;

}
