import { Component, OnInit } from '@angular/core';
import { MapService } from '../services/map.service';
import { LatLongNew } from '../model/lat-long-new';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {


  public zoom = 6  ;
  public opacity = 1.0;
  public width = 5;
  paths: Array<any> = new Array<any>();
  securePaths: Array<any> = new Array<any>();
  showMenuImage = false;
  showStucked = false;
  showHealthy = true;

  // paths: Array<any> = [
  //   {lat: -1.26506570332151, lng: 39.2191184313177},
  //   {lat: -1.54270354592336, lng: 34.0178536278542},
  //   {lat: 0.0327493128454535, lng: 31.3787860937591},
  //   {lat: -2.70208911969814, lng: 27.3001276591711},
  //   {lat: -4.14911123590855, lng: 36.280305106561},
  //   {lat: -9.40606261236155, lng: 39.3274715414288},
  //   {lat: -3.92356520344458, lng: 41.7750300623909},
  //   {lat: -1.26506570332151, lng: 39.2191184313177}];



  // paths: Array<any> = [
  //   { lat: 0, lng: 10 },
  //   { lat: 0, lng: 20 },
  //   { lat: 10, lng: 20 },
  //   { lat: 10, lng: 10 },
  //   { lat: 0, lng: 10 }
  //   ];

//  paths: Array<any> = [];


//  paths: Array<any> = [{lat: 5.2744, lng: 33.9475},
//  {lat: 6.0306, lng: 32.8915},
//  {lat: 5.2375, lng: 26.7815},
//  {lat: 4.3182, lng: 30.7912},
//  {lat: 4.1898, lng: 31.2665},
//  {lat: 0.3018, lng: 24.9311},
//  {lat: 2.8056, lng: 29.6459},
//  {lat: 3.3585, lng: 33.9764},
//  {lat: -2.911, lng: 39.5265},
//  {lat: 3.7412, lng: 37.7157},
//  {lat: 5.2744, lng: 33.9475}];


// paths: Array<any> = [{lat: 38.8837, lng: 22.7771},
// {lat: 33.9971, lng: 18.917},
// {lat: 34.3067, lng: 15.2877},
// {lat: 35.4525, lng: 11.3495},
// {lat: 34.6796, lng: 11.0088},
// {lat: 33.5659, lng: 11.1602},
// {lat: 27.9217, lng: 12.9875},
// {lat: 24.4647, lng: 14.881},
// {lat: 24.2018, lng: 20.2556},
// {lat: 27.7233, lng: 26.5847},
// {lat: 38.8837, lng: 22.7771}];


  constructor(private mapService: MapService) { }

  ngOnInit() {
   this.mapService.getPointsMonitored().then(resp => {

     resp.forEach( coordinate => {
       this.paths.push({lat: coordinate.lat, lng: coordinate.lng, type: coordinate.type});
    });

    this.mapService.getSecurePointsMonitored().then(respSecure => {

      respSecure.forEach( coordinateSecure => {
        this.securePaths.push({lat: coordinateSecure.lat, lng: coordinateSecure.lng, type: coordinateSecure.type});
        console.log(this.securePaths);
     });
    });
  });
}

showMenu() {
  this.showMenuImage = true;
}

closeMenuImage() {
  this.showMenuImage = false;
  this.showStuckedClick();
}

showStuckedClick() {
  this.showHealthy = false;
  this.showStucked = true;
}

showGoEnabled(){
  this.showHealthy = true;
  this.showStucked = false;

}



    // this.paths = resp.map(coordinate => ({ lat: coordinate[0], lng: coordinate[1] }));



    // this.paths =  resp.coordinates;

    }
